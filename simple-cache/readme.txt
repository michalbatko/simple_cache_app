Simple Cache

After checkout, build with Maven:
    mvn clean install
and in /target directory run:
    java -jar simple-cache-1.0-SNAPSHOT.jar

REST interface:
    GET:    http://localhost:8080/cache/content/my-key    - retrieving value stored under key: my-key
    PUT:    http://localhost:8080/cache/content/my-key    - adding new value under key: my-key
    DELETE: http://localhost:8080/cache/content/my-key    - delete value stored under key: my-key
    DELETE: http://localhost:8080/cache/content           - invalidate cache and delete all stored values

By default, as defined in config.properties file, cache entries live 60sec (param: element.expiry.time)
and are checked for expiration every 1sec (param: cache.expiry.check.time).
