package pl.mbatko.simplecache.controller;

import pl.mbatko.simplecache.cache.Cache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(MockitoJUnitRunner.class)
public class CacheControllerTest {

    private MediaType contentType = new MediaType(MediaType.TEXT_PLAIN.getType(),
            MediaType.TEXT_PLAIN.getSubtype(),
            Charset.forName("utf8"));

    private static final String CACHE_URL = "/cache/content";

    @Mock
    private Cache cache;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = standaloneSetup(new CacheController(cache)).build();
    }

    @Test
    public void shouldGetValueFromCache() throws Exception {
        //given
        String url = CACHE_URL + "/my-key";
        String key = "my-key";
        String value = "value";

        when(cache.get(key)).thenReturn(Optional.of(value));

        //then
        this.mockMvc.perform(get(url).accept(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(content().string(value));

        verify(cache).get(key);
        verifyNoMoreInteractions(cache);
    }

    @Test
    public void shouldGetErrorWhenNoValueInCache() throws Exception {
        //given
        String url = CACHE_URL + "/my-key";
        String key = "my-key";

        when(cache.get(key)).thenReturn(Optional.empty());

        //then
        this.mockMvc.perform(get(url).accept(contentType))
                .andExpect(status().isNotFound());

        verify(cache).get(key);
        verifyNoMoreInteractions(cache);
    }

    @Test
    public void shouldPutValueToCache() throws Exception {
        //given
        String url = CACHE_URL + "/my-key";
        String value = "value";
        String key = "my-key";

        //then
        this.mockMvc.perform(put(url).content(value).accept(contentType))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", url));

        verify(cache).put(key, value);
        verifyNoMoreInteractions(cache);
    }

    @Test
    public void shouldDeleteValueFromCache() throws Exception {
        //given
        String url = CACHE_URL + "/my-key";
        String key = "my-key";

        //then
        this.mockMvc.perform(delete(url).accept(contentType))
                .andExpect(status().isOk());

        verify(cache).remove(key);
        verifyNoMoreInteractions(cache);
    }

    @Test
    public void shouldInvalidateCache() throws Exception {
        this.mockMvc.perform(delete(CACHE_URL).accept(contentType))
                .andExpect(status().isOk());

        verify(cache).invalidate();
        verifyNoMoreInteractions(cache);
    }
}