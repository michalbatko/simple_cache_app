package pl.mbatko.simplecache.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableAutoConfiguration
@ComponentScan({"pl.mbatko.simplecache.cache", "pl.mbatko.simplecache.controller"})
@PropertySource("test.config.properties")
@EnableScheduling
public class TestConfiguration {
}
