package pl.mbatko.simplecache.integration;

import pl.mbatko.simplecache.config.TestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(TestConfiguration.class)
@WebIntegrationTest("server.port:9000")
public class SolutionTest {

    public static final String BASE_URL = "http://localhost:9000/cache/content";
    RestTemplate template = new TestRestTemplate();


    @Test
    public void shouldGetErrorWhenNoValueInCache() throws Exception {
        //given
        String url = BASE_URL + "/my-key1";

        //when
        ResponseEntity<String> response = template.getForEntity(url, String.class);

        //then
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void shouldGetValueExistingInCache() throws Exception {
        //given
        String url = BASE_URL + "/my-key2";
        String value = "value";

        //when
        template.put(url, value);
        String result = template.getForObject(url, String.class);

        //then
        assertEquals(value, result);
    }

    @Test
    public void shouldDeleteValueFromCache() throws Exception {
        //given
        String url = BASE_URL + "/my-key3";
        String value = "value";

        //when
        template.put(url, value);
        String before = template.getForObject(url, String.class);
        template.delete(url);
        String after = template.getForObject(url, String.class);

        //then
        assertEquals(before, value);
        assertNull(after);
    }

    @Test
    public void shouldInvalidateCache() throws Exception {
        //given
        String cacheUrl = BASE_URL;
        String url1 = cacheUrl + "/my-key4";
        String url2 = cacheUrl + "/my-key5";
        String url3 = cacheUrl + "/my-key6";

        String value = "value";

        //when
        template.put(url1, value);
        template.put(url2, value);
        template.put(url3, value);
        String before1 = template.getForObject(url1, String.class);
        String before2 = template.getForObject(url2, String.class);
        String before3 = template.getForObject(url3, String.class);

        template.delete(cacheUrl);
        String after1 = template.getForObject(url1, String.class);
        String after2 = template.getForObject(url2, String.class);
        String after3 = template.getForObject(url3, String.class);

        //then
        assertEquals(before1, value);
        assertEquals(before2, value);
        assertEquals(before3, value);
        assertNull(after1);
        assertNull(after2);
        assertNull(after3);
    }

    @Test
    public void shouldRemoveExpiredEntries() throws Exception {
        //given
        String cacheUrl = BASE_URL;
        String url1 = cacheUrl + "/my-key7";
        String url2 = cacheUrl + "/my-key8";
        String value = "value";

        //when
        template.put(url1, value);
        String firstInserted = template.getForObject(url1, String.class);

        Thread.sleep(150);
        template.put(url2, value);
        String secondInserted = template.getForObject(url2, String.class);

        Thread.sleep(250);
        String firstExists = template.getForObject(url1, String.class);

        Thread.sleep(350);
        String firstStillExists = template.getForObject(url1, String.class);
        String secondYetExpired = template.getForObject(url2, String.class);

        Thread.sleep(700);
        String firstFinallyExpired = template.getForObject(url1, String.class);
        String secondFinallyExpired = template.getForObject(url2, String.class);

        //then
        assertEquals(value, firstInserted);
        assertEquals(value, secondInserted);
        assertEquals(value, firstExists);
        assertEquals(value, firstStillExists);
        assertNull(secondYetExpired);
        assertNull(firstFinallyExpired);
        assertNull(secondFinallyExpired);
    }
}
