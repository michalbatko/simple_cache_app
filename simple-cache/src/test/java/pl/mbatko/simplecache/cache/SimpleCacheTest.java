package pl.mbatko.simplecache.cache;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class SimpleCacheTest {

    private Cache cache;

    @Before
    public void setUp() throws Exception {
        cache = new SimpleCache(500);
    }

    @Test
    public void shouldPutAndGetData() {
        //given
        String key = "key";
        String value = "value";

        //when
        Optional<String> before = cache.get(key);
        cache.put(key, value);
        Optional<String> after = cache.get(key);

        //then
        assertFalse(before.isPresent());
        assertEquals(value, after.get());
    }

    @Test
    public void shouldRemoveData() {
        //given
        String key = "key";
        String value = "value";

        //when
        cache.put(key, value);
        Optional<String> before = cache.get(key);
        cache.remove(key);
        Optional<String> after = cache.get(key);

        //then
        assertEquals(value, before.get());
        assertFalse(after.isPresent());
    }

    @Test
    public void shouldInvalidateData() {
        //given
        String value = "value";

        //when
        cache.put("key1", value);
        cache.put("key2", value);
        cache.put("key3", value);
        cache.invalidate();
        Optional<String> value1 = cache.get("key1");
        Optional<String> value2 = cache.get("key2");
        Optional<String> value3 = cache.get("key3");

        //then
        assertFalse(value1.isPresent());
        assertFalse(value2.isPresent());
        assertFalse(value3.isPresent());
    }

    @Test
    public void shouldRemoveExpiredData() throws InterruptedException {
        //given
        String key = "key";
        String value = "value";

        //when
        cache.put(key, value);
        cache.removeExpired();
        Optional<String> before = cache.get(key);
        Thread.sleep(700);
        cache.removeExpired();
        Optional<String> after = cache.get(key);

        //then
        assertEquals(value, before.get());
        assertFalse(after.isPresent());
    }

    @Test
    public void shouldExtendPresenceOfDataInCache() throws InterruptedException {
        //given
        String firstKey = "firstKey";
        String secondKey = "secondKey";
        String value = "value";

        //when
        cache.put(firstKey, value);
        cache.put(secondKey, value);

        cache.removeExpired();
        Optional<String> firstKeyExists = cache.get(firstKey);
        Optional<String> secondKeyExists = cache.get(secondKey);

        Thread.sleep(300);
        cache.removeExpired();
        Optional<String> firstKeyExtended = cache.get(firstKey);

        Thread.sleep(300);
        cache.removeExpired();
        Optional<String> firstKeyExtendedAgain = cache.get(firstKey);
        Optional<String> secondKeyExpired = cache.get(secondKey);

        Thread.sleep(600);
        cache.removeExpired();
        Optional<String> firstKeyFinallyExpired = cache.get(firstKey);
        Optional<String> secondKeyFinallyExpired = cache.get(secondKey);

        //then
        assertEquals(value, firstKeyExists.get());
        assertEquals(value, secondKeyExists.get());

        assertEquals(value, firstKeyExtended.get());

        assertEquals(value, firstKeyExtendedAgain.get());
        assertFalse(secondKeyExpired.isPresent());

        assertFalse(firstKeyFinallyExpired.isPresent());
        assertFalse(secondKeyFinallyExpired.isPresent());
    }
}