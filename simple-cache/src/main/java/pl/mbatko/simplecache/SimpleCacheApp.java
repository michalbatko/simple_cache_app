package pl.mbatko.simplecache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableAutoConfiguration
@ComponentScan("pl.mbatko.simplecache")
@PropertySource("config.properties")
@EnableScheduling
public class SimpleCacheApp {
    public static void main(String[] args) {
        SpringApplication.run(SimpleCacheApp.class, args);
    }
}
