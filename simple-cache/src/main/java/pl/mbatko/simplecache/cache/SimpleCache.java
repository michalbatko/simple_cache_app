package pl.mbatko.simplecache.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implementing cache behavior using ConcurrentHashMap with custom expiration logic.
 * But there is a nice CacheBuilder in Guava, that can be used instead:
 * http://docs.guava-libraries.googlecode.com/git/javadoc/com/google/common/cache/CacheBuilder.html
 */
@Component
public class SimpleCache implements Cache {

    private Map<String, Entry> map = new ConcurrentHashMap<>();
    private Integer expiryTime;

    @Autowired
    public SimpleCache(@Value("${element.expiry.time}") Integer expiryTimeMillis) {
        this.expiryTime = expiryTimeMillis;
    }

    public Optional<String> get(String key) {
        Entry entry = map.get(key);
        if (entry != null) {
            entry.refresh();
            return Optional.of(entry.value);
        }

        return Optional.empty();
    }

    public void put(String key, String data) {
        map.put(key, new Entry(data));
    }

    public void remove(String key) {
        map.remove(key);
    }

    public void invalidate() {
        map.clear();
    }

    @Scheduled(fixedRateString = "${cache.expiry.check.time}")
    public void removeExpired() {
        long timestamp = System.currentTimeMillis();
        map.entrySet().removeIf(entry -> timestamp - entry.getValue().timestamp > expiryTime);
    }

    private class Entry {
        String value;
        long timestamp;

        public Entry(String value) {
            this.value = value;
            refresh();
        }

        public void refresh() {
            this.timestamp = System.currentTimeMillis();
        }
    }
}
