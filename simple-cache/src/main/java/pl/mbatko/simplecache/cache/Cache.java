package pl.mbatko.simplecache.cache;

import java.util.Optional;

public interface Cache {

    Optional<String> get(String key);

    void put(String key, String data);

    void remove(String key);

    void invalidate();

    void removeExpired();
}
