package pl.mbatko.simplecache.controller;

import pl.mbatko.simplecache.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URI;
import java.util.Optional;

@Controller
@EnableAutoConfiguration
@RequestMapping(CacheController.URI_MAPPING)
public class CacheController {

    static final String URI_MAPPING = "/cache/content";

    private Cache cache;

    @Autowired
    public CacheController(Cache cache) {
        this.cache = cache;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{key}")
    ResponseEntity<?> getData(@PathVariable String key) {
        Optional<String> data = cache.get(key);
        if (!data.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(data.get());
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{key}")
    ResponseEntity<Void> addData(@PathVariable String key, @RequestBody String data) {
        cache.put(key, data);
        URI location = URI.create(URI_MAPPING + "/" + key);
        return ResponseEntity.created(location).build();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{key}")
    ResponseEntity<Void> removeData(@PathVariable String key) {
        cache.remove(key);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    ResponseEntity<Void> invalidate() {
        cache.invalidate();
        return ResponseEntity.ok().build();
    }
}
